=== Envo's Elementor Templates & Widgets for WooCommerce ===
Contributors: EnvoThemes
Tags: elementor, woocommerce, templates, woo, widgets, drag-and-drop, e-commerce, elements, landing page, elementor widget, woocommerce elementor, product
Requires at least: 4.4
Tested up to: 5.4.2
Stable tag: 1.0
Requires PHP: 5.4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Elementor Templates & Widgets for WooCommerce

== Description ==
Envo's Elementor Templates & Widgets for WooCommerce is a WooCommerce addon for Elementor Page Builder. It comes with free multiple Elementor templates designed for WooCommerce and multiple Elementor widgets, including WooCommerce products displayed in grid or carousel layout. 

== Elementor WooCommerce templates ==
* 5 homepage Elementor templates
* 2 contact Elementor templates
* 2 faq Elementor page template
* More comming soon

You can see the demo pages here : [Live Demo](https://envothemes.com/elementor-templates-for-woocommerce/)

== Elementor Widgets for WooCommerce ==
* WooCommerce Carousel Widget - WooCommerce products in carousel mode
* WooCommerce Products Widget - WooCommerce products in grid mode
* WooCommerce Categories Widget - WooCommerce categories in grid mode
* WooCommerce Add To Cart Widget - WooCommerce add to cart button for selected product
* WooCommerce Ajax Search Widget - Ajax search filed to search products (or posts/pages) live, without page redirection
* Contact Form 7 Widget - Style contact forms from popular free plugin Contact Form 7
* Tabs Widget - Tabs with multiple layout options
* Price Table Widget - Pricing table widget with multiple design options
* Off Canvas Button Widget - Open off canvas block, where you can show your sidebar or custom Elementor template
* Flip Box Widget - Animated widget to show back side of box on hover
* Blog Grid Widget - Show your blog feed
* Animated Heading Widget - Heading with beautiful animations

== Features ==
* Multiple WooCommerce Elementor templates
* 12 Elementor widgets 
* WooCommerce Elementor widgets
* WooCommerce featured, recent, on sale or custom product select with multiple include/exclude options
* Unlimited colors and font variants
* Products styling
* Products carousel


== Installation ==
This section describes how to install the Elementor Templates & Widgets for WooCommerce plugin and get it working.

= 1) Install =

1. Go to the WordPress Dashboard "Add New Plugin" section.
2. Search For "Envo's Elementor Templates & Widgets for WooCommerce".
3. Install, then Activate it.

= OR =
1. Unzip (if zipped) and Upload `elementor-templates-widgets-woocommerce` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

= 2) Import =
1. After you install and activate the plugin you will get a notice to install Elementor/WooCommerce Plugin ( If allready install it then do not show notice. ).
2. To install the plugin click on the "Button" Install Elementor/WooCommerce.
3. Now go to WooCommerce - Templates library to import the Elementor templates

= 3) Elementor Widgets =
1. All our WooCommerce Elementor Widgets are available in Elementor builder under "ETWW Elements".

== Frequently Asked Questions ==
= Does it work with any WordPress theme? =

Yes, it will work with any WordPress theme as long as you are using WooCommerce and Elementor as a page builder. However, some dynamic elements, such as a product or product category, appear in a different layout that is defined by the theme. Some themes override WooCommerce product styling and this can make some widget options inactive.

= My WooCommerce products look different as in the demo. =

The products or categories styling is loaded from the theme enabled on your website. Each theme that supports WooCommerce can have own products styling. Our demo is loaded with free Envo Storefront theme.

= Some product widget options not work? =

Your theme override the WooCommerce styling, and this can make some widget options inactive. In this case we are soory, but we can not help you. You can try different theme.

= Is the Elementor PRO required? =

No, Elementor PRO or any other PRO addon is not required.

== Screenshots ==

1. WooCommerce Elementor Homepage Template #1
2. WooCommerce Elementor Homepage Template #2
3. WooCommerce Elementor Homepage Template #3
4. WooCommerce Elementor Homepage Template #4
5. WooCommerce Elementor Homepage Template #5
6. WooCommerce Elementor Contact Template #1
7. WooCommerce Elementor Contact Template #2
8. WooCommerce Elementor FAQ Template #1
9. WooCommerce Elementor FAQ Template #2
10. WooCommerce Elementor Widgets

== Changelog ==

= Version: 1.0 =
* Initial version
